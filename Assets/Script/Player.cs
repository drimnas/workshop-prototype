﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float moveInput;

    public float speed;

    public bool isGrounded;

    public Rigidbody2D rb;

    public float force;

    public float jumpTimer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        moveInput = Input.GetAxis("Horizontal");

      //  if (isGrounded)
      //  {
            transform.Translate(new Vector2(moveInput * speed , 0) * Time.deltaTime);
     //   }

       /* if (isGrounded == false && rb.velocity.x <= speed)
        {
            transform.Translate(new Vector2(moveInput * speed/ 2 , 0) * Time.deltaTime); 
        }*/


        if (jumpTimer <= 0)
        {
            
            if (isGrounded && Input.GetButtonDown("Jump"))
            {
                rb.AddForce(new Vector2(0, force), ForceMode2D.Impulse);
                isGrounded = false;
                jumpTimer = 0.35f;
            }
        }


        if (jumpTimer >= 0)
        {
            jumpTimer -= Time.deltaTime;
        }

    }


    
    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Terrain"))
        {
            isGrounded = true;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Terrain"))
        {
            isGrounded = false;
        }
    }
}
